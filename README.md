CodeJam2017
===========


### Qualification Round  
Rank: 6959  
Score: 50  
Correct: A-large, B-large, C-small-1, C-small-2  
https://code.google.com/codejam/contest/3264486/scoreboard#sp=6931  


### Round 1B   
Rank: 6387  
Score: 25  
Correct: A-large  
https://code.google.com/codejam/contest/8294486/scoreboard?c=8294486#sp=6361  