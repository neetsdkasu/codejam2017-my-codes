package main

import "testing"

func TestSolve(t *testing.T) {
	cases := []struct {
		s      string
		k, ans int
		ok     bool
	}{
		{"---+-++-", 3, 3, true},
		{"+++++", 4, 0, true},
		{"-+-+-", 4, 0, false},
	}
	for i, c := range cases {
		ans, ok := Solve(c.s, c.k)
		if ok != c.ok {
			t.Errorf("case %d: miss\n", i)
		} else if ok {
			if ans != c.ans {
				t.Errorf("case %d: unmatch: %d <> %d", i, ans, c.ans)
			}
		}
	}
}
