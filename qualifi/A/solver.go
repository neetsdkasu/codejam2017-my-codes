package main

import "fmt"

func Solve(s string, k int) (ans int, ok bool) {
	runes := ([]rune)(s)
	for i, c1 := range runes[:len(runes)-k+1] {
		if c1 == '-' {
			ans++
			for j, c2 := range runes[i : i+k] {
				if c2 == '-' {
					runes[i+j] = '+'
				} else {
					runes[i+j] = '-'
				}
			}
		}
	}
	for _, c := range runes[len(runes)-k:] {
		if c == '-' {
			return
		}
	}
	ok = true
	return
}

func main() {
	var t int
	fmt.Scan(&t)
	for i := 0; i < t; i++ {
		fmt.Printf("Case #%d: ", i+1)
		var s string
		var k int
		fmt.Scan(&s, &k)
		if ans, ok := Solve(s, k); ok {
			fmt.Println(ans)
		} else {
			fmt.Println("IMPOSSIBLE")
		}
	}
}
