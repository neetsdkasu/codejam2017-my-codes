package main

import "testing"

func TestSolve(t *testing.T) {
	cases := []struct {
		n, ans int64
	}{
		{132, 129},
		{1000, 999},
		{7, 7},
		{111111111111111110, 99999999999999999},
		{1000000000000000000, 999999999999999999},
	}
	for i, c := range cases {
		ans := Solve(c.n)
		if ans != c.ans {
			t.Errorf("case %d: unmatch: %d <> %d", i, ans, c.ans)
		}
	}
}
