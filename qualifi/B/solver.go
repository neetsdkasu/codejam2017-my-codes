package main

import "fmt"

func Solve(n int64) (ans int64) {
	x := n
	ans = n
	i := 0
	p := 0
	for x > 9 {
		i++
		k1 := x % 10
		x = x / 10
		k2 := x % 10
		if k2 > k1 {
			x--
			p = i
			ans = x
		}
	}
	for j := 0; j < p; j++ {
		ans = ans*10 + 9
	}
	return
}

func main() {
	var t int
	fmt.Scan(&t)
	for i := 0; i < t; i++ {
		fmt.Printf("Case #%d: ", i+1)
		var n int64
		fmt.Scan(&n)
		ans := Solve(n)
		fmt.Println(ans)
	}
}
