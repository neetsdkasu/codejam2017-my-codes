package main

import "testing"

func test(t *testing.T, tf func(int64, int64) (int64, int64)) {
	cases := []struct {
		n, k, max, min int64
	}{
		{4, 2, 1, 0},
		{5, 2, 1, 0},
		{6, 2, 1, 1},
		{1000, 1000, 0, 0},
		{1000, 1, 500, 499},
		{1000000, 1000000, 0, 0},
	}
	for i, c := range cases {
		max, min := tf(c.n, c.k)
		if max != c.max {
			t.Errorf("case %d: unmatch: max %d <> %d", i, max, c.max)
		}
		if min != c.min {
			t.Errorf("case %d: unmatch: min %d <> %d", i, min, c.min)
		}
	}
}

func TestSolve1000(t *testing.T) {
	test(t, func(n, k int64) (int64, int64) {
		if n > 1000 {
			t.Skipf("over 1000")
			return -1, -1
		}
		return Solve1000(n, k)
	})
}

func TestSolve(t *testing.T) {
	test(t, Solve)
}
