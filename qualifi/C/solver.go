package main

import (
	"container/heap"
	"fmt"
)

type Range struct {
	Min, Max int64
}

func NewRange(size int64) Range {
	size--
	tmp := size / 2
	return Range{tmp, size - tmp}
}

func (r Range) Less(x Range) bool {
	if r.Min > x.Min {
		return true
	}
	if r.Min < x.Min {
		return false
	}
	return r.Max > x.Max
}

type PriorityQueue []Range

func (pq PriorityQueue) Len() int            { return len(pq) }
func (pq PriorityQueue) Less(i, j int) bool  { return pq[i].Less(pq[j]) }
func (pq PriorityQueue) Swap(i, j int)       { pq[i], pq[j] = pq[j], pq[i] }
func (pq *PriorityQueue) Push(x interface{}) { *pq = append(*pq, x.(Range)) }
func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	tmp := old[n-1]
	*pq = old[:n-1]
	return tmp
}

func Solve1000(n, k int64) (max, min int64) {
	pq := make(PriorityQueue, 0, 2000)
	pq = append(pq, NewRange(n))
	heap.Init(&pq)
	for i := int64(1); i < k; i++ {
		x := heap.Pop(&pq).(Range)
		if x.Min > 0 {
			heap.Push(&pq, NewRange(x.Min))
		}
		if x.Max > 0 {
			heap.Push(&pq, NewRange(x.Max))
		}
	}
	if len(pq) == 0 {
		return -1, -1
	}
	x := heap.Pop(&pq).(Range)
	max, min = x.Max, x.Min
	return
}

func Solve(n, k int64) (max, min int64) {
    return Solve1000(n, k)
}

func main() {
	var t int
	fmt.Scan(&t)
	for i := 0; i < t; i++ {
		fmt.Printf("Case #%d: ", i+1)
		var n, k int64
		fmt.Scan(&n, &k)
		max, min := Solve(n, k)
		fmt.Println(max, min)
	}
}
