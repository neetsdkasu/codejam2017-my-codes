package main

import (
	"math"
	"testing"
)

func TestSolve(t *testing.T) {
	cases := []struct {
		d   float64
		h   []Horse
		ans float64
	}{
		{2525, []Horse{
			{2400, 5},
		}, 101.0},
		{300, []Horse{
			{120, 60},
			{60, 90},
		}, 100.0},
		{100, []Horse{
			{80, 100},
			{70, 10},
		}, 100.0 / 3.0},
	}
	for i, c := range cases {
		ans := Solve(c.d, c.h)
		diff := math.Abs(ans - c.ans)
		if !(diff < 1.0e-6) {
			t.Errorf("case %d: unmatch: %v <> %v", i, c.ans, ans)
		}
	}
}
