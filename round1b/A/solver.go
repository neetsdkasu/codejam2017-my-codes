package main

import (
    "fmt"
    "math"
)

type Horse struct {
	K, S float64
}

func Solve(d float64, h []Horse) (ans float64) {
    var max float64
    for _, x := range h {
        max = math.Max(max, (d - x.K) / x.S)
    }
    ans = d / max
	return
}

func main() {
	var t int
	fmt.Scan(&t)
	for i := 0; i < t; i++ {
		fmt.Printf("Case #%d: ", i+1)
		var d float64
		var n int
		fmt.Scan(&d, &n)
		h := make([]Horse, n)
		for i, _ := range h {
			fmt.Scan(&h[i].K, &h[i].S)
		}
		ans := Solve(d, h)
		fmt.Println(ans)
	}
}
